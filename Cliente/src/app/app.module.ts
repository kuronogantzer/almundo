import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

/*MATERIAL COMPONENTS*/
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatTableModule,
    MatIconModule } from '@angular/material';

import { APP_ROUTING } from './app.routes';

import { HotelService } from './services/hotel.service';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { HotelComponent } from './components/hotel/hotel.component';
import { HomeComponent } from './components/home/home.component';
import { NumberArrayPipe } from './pipes/number-array.pipe';


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        HotelComponent,
        HomeComponent,
        NumberArrayPipe
    ],
    imports: [
        BrowserModule,
        RouterModule,
        APP_ROUTING,
        HttpClientModule,
        FormsModule,
        /*MATERIAL COMPONENTS*/
        MatExpansionModule,
        BrowserAnimationsModule,
        MatIconModule,
        MatCheckboxModule
    ],
    providers: [ HotelService ],
    bootstrap: [AppComponent]
})
export class AppModule { }
