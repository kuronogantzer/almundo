export interface IHotel {
    id: string;
    name: string;
    image: string;
    price: number;
    stars: number;
    amenities: string[];
}
export class Hotel implements IHotel {
    id: string;
    name: string;
    image: string;
    price: number;
    stars: number;
    amenities: string[];
}
