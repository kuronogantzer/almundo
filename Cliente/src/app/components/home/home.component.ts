import { Component, OnInit } from '@angular/core';
import { HotelService } from '../../services/hotel.service';
import { Hotel, IHotel } from '../../models/Hotel';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styles: []
})
export class HomeComponent implements OnInit {

    public filter: string;
    public stars: string = 'all';
    public checkAllStars: boolean = true;
    public checkStar2: boolean = false;
    public checkStar1: boolean = false;
    public checkStar3: boolean = false;
    public checkStar4: boolean = false;
    public checkStar5: boolean = false;
    public hotels: IHotel[] = [];
    public hotelsFiltered: IHotel[] = [];

    constructor(public _hotelService: HotelService) {
        this._hotelService.getHotels().subscribe( (data: IHotel[]) => {
            this.hotels = data;
            Object.assign(this.hotelsFiltered, this.hotels);
        });
    }

    ngOnInit() {
    }

    filterHotels(event:any, ignoreEvent:boolean = false) {
        if(event.keyCode === 13 || ignoreEvent) {
            if((this.filter && this.filter !== "") || (this.stars && this.stars !== "")) {
                this._hotelService.getHotelsByFilter(this.filter, this.stars)
                    .subscribe( (data: IHotel[]) => {
                        this.hotelsFiltered = data;
                    });
            }
            else
                Object.assign(this.hotelsFiltered, this.hotels);
        }
    }

    modifyStars() {
        this.stars = "";
        if(this.checkStar1)
            this.stars = this.stars + '1';
        else {
            let index = this.stars.indexOf('1');
            this.stars = this.stars.substring(0, index-1) + this.stars.substring(index+1, this.stars.length);
        }
        if(this.checkStar2)
            this.stars = this.stars + '2';
        else {
            let index = this.stars.indexOf('2');
            this.stars = this.stars.substring(0, index-1) + this.stars.substring(index+1, this.stars.length);
        }
        if(this.checkStar3)
            this.stars = this.stars + '3';
        else {
            let index = this.stars.indexOf('3');
            this.stars = this.stars.substring(0, index-1) + this.stars.substring(index+1, this.stars.length);
        }
        if(this.checkStar4)
            this.stars = this.stars + '4';
        else {
            let index = this.stars.indexOf('4');
            this.stars = this.stars.substring(0, index-1) + this.stars.substring(index+1, this.stars.length);
        }
        if(this.checkStar5)
            this.stars = this.stars + '5';
        else {
            let index = this.stars.indexOf('5');
            this.stars = this.stars.substring(0, index-1) + this.stars.substring(index+1, this.stars.length);
        }
        if(this.stars === "12345") {
            this.stars = "all";
            this.checkAllStars = true;
        }
        else
            this.checkAllStars = false;
    }

    setAllStars() {
        if(this.checkAllStars) {
            this.stars = "all";
            this.checkStar1 = false;
            this.checkStar2 = false;
            this.checkStar3 = false;
            this.checkStar4 = false;
            this.checkStar5 = false;
        }
    }

}
