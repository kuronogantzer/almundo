import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map'

@Injectable()
export class HotelService {

    private  urlAPI: string = 'http://localhost:3000/';
    constructor(private http: HttpClient) { }

    getHotels() {
        return this.http.get(this.urlAPI + 'getHotels');
    }

    getHotelsByFilter(filter, stars) {
        return this.http.get(this.urlAPI + 'getHotelsByFilter?filter=' + (filter || '') + '&stars=' + (stars || ''));
    }

}
