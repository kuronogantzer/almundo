module.exports = function HotelData() {

    this.fs = require("fs");
    this.dataHotel = 'data.json';
    // var persistence = require('./data-persistence.js');
    // this.data = new persistence();
    //
    // this.init = function() {
    //     this.data.init();
    // }

    this.readData = function () {
        return this.fs.readFileSync(this.dataHotel, (err, data) => {
            if(err){
                console.log("Error reading the data: " + err);
            }
            return data;
        });
    };

    this.getHotels = function () {
        var result = this.readData();
        return JSON.parse(result);
    };

    this.getHotelsByFilter = function (filter, stars) {
        var hotels = this.readData();
        if(!stars || stars === "all") {
            return JSON.parse(hotels).filter( function (hotel) {
                return hotel.name.toUpperCase().includes(filter.toUpperCase());
            });
        }
        else if (!filter || filter === "") {
            return JSON.parse(hotels).filter( function (hotel) {
                return stars.includes(hotel.stars.toString());
            });
        }
        else if(stars && filter && stars !== "" && filter !== "") {
            return JSON.parse(hotels).filter( function (hotel) {
                return (hotel.name.toUpperCase().includes(filter.toUpperCase())
                && stars.includes(hotel.stars.toString()));
            });
        }
        else {
            return JSON.parse(hotels);
        }
    };

    this.getHotelById = function (id) {
        var hotels = this.readData();
        return JSON.parse(hotels).filter( function (hotel) {
            return hotel.id === id;
        })[0];
    };

    this.addNewHotel = function (newHotel) {
        var hotels = this.readData();
        hotels = JSON.parse(hotels);
        var maxId = 0;
        hotels.forEach( (hotel) => {
            if(hotel.id > maxId)
            maxId = hotel.id;
        });
        newHotel.id = maxId + 1;

        this.fs.appendFileSync(this.dataHotel, JSON.stringify(newHotel), (error, res) => {
            if(error){
                console.log('Error adding new hotel: ' + error);
                return false;
            }
            return true;
        });
        return true;
    };

    this.updateHotelById = function (modifiedHotel) {
        var prevHotel = getHotelById(modifiedHotel.id);
        if(prevHotel) {
            prevHotel.id = modifiedHotel.id;
            prevHotel.name = modifiedHotel.name;
            prevHotel.satars = modifiedHotel.satars;
            prevHotel.price = modifiedHotel.price;
            prevHotel.image = modifiedHotel.image;
            prevHotel.amenities = modifiedHotel.amenities;
            this.fs.readFileSync(this.dataHotel, (err, data) => {
                if(err)
                console.log('Error reading data: ' + err);
                return true;
            });
        }
        else {
            console.log('The hotel was not found.');
            return false;
        }
    };
};

//module.exports = HotelData
