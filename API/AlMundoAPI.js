var express = require("express"),
    app = express(),
    bodyParser  = require("body-parser"),
    hotelData = require("./Hotel.data.js");
    hotels = new hotelData();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

/*Get all hotels*/
app.get('/getHotels', function(req, res, next) {
    res.send(hotels.getHotels());
});

/*Get hotels by filter*/
app.get('/getHotelsByFilter', function(req, res) {
    var filter = req.query.filter;
    var stars = req.query.stars;
    res.send(hotels.getHotelsByFilter(filter, stars));
});

/*Get hotels by Id*/
app.get('/getHotelById/:id', function(req, res, next) {
    var id = req.params.id;
    res.send(hotels.getHotelById(id));
});

/*Add new Hotel*/
app.post('/addNewHotel', function(req, res) {
    var hotel = req.body;
    res.send('Add Hotel ' + hotel.name);
});

/*Update Hotel by Id*/
app.put('/updateHotelById', function(req, res) {
    var hotel = req.body;
    res.send('Update Hotel ' + hotel.id + ' with ' + JSON.stringify(hotel));
});

/*Delete Hotel by Id*/
app.delete('/DeleteHotelById/:id', function(req, res) {
   var hotelId = req.params.id;
   res.send('Delete Hotel' + hotelId);
});

app.listen(3000, function() {
    //hotels.init();
    console.log("AlMundo server running on http://localhost:3000");
});
