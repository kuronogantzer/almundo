module.exports = function dataPersistence() {

    this.storage = require('node-persist');
    this.fs = require('fs');
    this.dataHotel = 'data.json';

    this.init = function() {
        return this.storage.init({
        	dir: 'persistence/',
        	stringify: JSON.stringify,
        	parse: JSON.parse,
        	encoding: 'utf8'
        }).then(function() {
            this.storage.getItem('Hotels').then(function(Hotels) {
                console.log('obteniendo data...');
                if(!Hotels) {
                    console.log('data no encontrada');
                    this.fs.readFile(this.dataHotel).then(function(data) {
                        console.log('insertando data');
                        this.storage.setItem(JSON.parse(data)).then(function(){}, function(){});
                    });
                }
                return Hotels;
            });
        });
    };


}
